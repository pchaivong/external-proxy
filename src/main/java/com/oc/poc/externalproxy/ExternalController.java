package com.oc.poc.externalproxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;

/**
 * Created by pchaivong on 11/7/2017 AD.
 */

@RestController
@RequestScope
public class ExternalController {


    private RestTemplate restTemplate;
    private EndpointConfiguration endpointConfiguration;

    @Autowired
    public ExternalController(RestTemplate restTemplate,
                              EndpointConfiguration endpointConfiguration){
        this.restTemplate = restTemplate;
        this.endpointConfiguration = endpointConfiguration;
    }


    @GetMapping(value = "/test/{id}")
    public ExternalResponseDTO externalAPI(@PathVariable long id){
        return restTemplate.getForObject(new StringBuilder()
                .append(endpointConfiguration.getUrl())
                .append("/")
                .append(id)
                .toString(), ExternalResponseDTO.class);
    }

}
