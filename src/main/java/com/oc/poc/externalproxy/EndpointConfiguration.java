package com.oc.poc.externalproxy;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by pchaivong on 11/7/2017 AD.
 */

@Configuration
@PropertySource("/data/application.yaml")
@ConfigurationProperties(prefix = "external")
public class EndpointConfiguration {


    private String url;

    public EndpointConfiguration() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
